"use strict";

// 1. DOM - это объектная модель документа, которую браузер создает в памяти компьютера на основании HTML-кода, полученного им от сервера. Иными словами, это представление HTML-документа в виде дерева тегов.
// 2. innerText — показывает всё текстовое содержимое, которое не относится к синтаксису HTML. То есть любой текст, заключённый между открывающими и закрывающими тегами элемента будет записан в innerText. Причём если внутри innerText будут ещё какие-либо элементы HTML со своим содержимым, то он проигнорирует сами элементы и вернёт их внутренний текст.
// innerHTML — покажет текстовую информацию ровно по одному элементу. В вывод попадёт и текст и разметка HTML-документа, которая может быть заключена между открывающими и закрывающими тегами основного элемента.
// 3. getElementById, getElementsByClassName, getElementsByName, getElementsByTagName, querySelector, querySelectorAll

const paragraphs = document.getElementsByTagName("p");
Array.from(paragraphs).forEach((paragraph) => {
  paragraph.style.backgroundColor = "#f00";
});

// Второй способ через querySelector

// const p = document.querySelectorAll("p");
// p.forEach((p) => {
//   p.style.backgroundColor = "#f00";
// });

const elementID = document.getElementById("optionsList");
console.log(elementID);
console.log(elementID.parentElement);

if (elementID.hasChildNodes()) {
  elementID.childNodes.forEach((item) => {
    console.log(item);
  });
}
const testParagraph = document.getElementById("testParagraph");
testParagraph.textContent = "This is a paragraph";

const header = document.querySelector(".main-header");
const headerArray = [...header.children];

headerArray.forEach((element) => {
  const li = element.getElementsByTagName("li");
  console.log(li);
  const liArray = Array.from(li);
  liArray.forEach((li) => {
    li.classList.add("nav-item");
    console.log(li);
  });
});

const sectionTitle = Array.from(
  document.getElementsByClassName("section-title")
);
console.log(sectionTitle);

sectionTitle.forEach((element) => {
  element.classList.remove("section-title");
  console.log(element);
});
